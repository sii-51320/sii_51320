#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h> 
#include <unistd.h>

#include "DatosMemCompartida.h"

int main() {



	//Declaracion de una variable de tipo puntero a DatosMemCompartida
	DatosMemCompartida* pMemC;

	//Abrimos el fichero proyectado en memoria creado anteriormente en el tenis y proyectado en 		memoria
	int file;
	char* org;
	file=open("ficheroMemoria",O_RDWR);
	org=(char*)mmap(NULL,sizeof(*(pMemC)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);



	//Cerrar el descriptor de fichero
	close(file);



	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero
	pMemC=(DatosMemCompartida*)org;



	//Bucle infinito
	while(1){

		float posiciony=(pMemC->raqueta1.y2+pMemC->raqueta1.y1)/2;//posicion del punto medio de la raqueta
		if (posiciony<pMemC->esfera.centro.y)
			pMemC->accion=1;
		else if (posiciony>pMemC->esfera.centro.y)
			pMemC->accion=-1;
		else
			pMemC->accion=0;
		//Suspender durente 25ms utilizando la llamada a
		usleep(250);



	}



	/*borra las ubicaciones para el rango de direcciones especificado, y produce referencias a las direcciones



	dentro del rango a fin de generar referencias a memoria inválidas.*/



	munmap(org,sizeof(*(pMemC)));



}
